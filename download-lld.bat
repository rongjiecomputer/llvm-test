appveyor DownloadFile https://ci.appveyor.com/api/projects/rongjiecomputer/%LLD_BASE%/artifacts/lld-standalone.7z -FileName lld-standalone.7z

7z x lld-standalone.7z -oC:\projects\deps\lld > nul
copy C:\projects\deps\lld\bin\lld.exe C:\projects\deps\lld\bin\lld-link.exe

set PATH=C:\projects\deps\lld\bin;%PATH%

rm lld-standalone.7z
