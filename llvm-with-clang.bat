git clone --depth 1 https://git.llvm.org/git/llvm.git C:\projects\src

set CLANG_BASE=llvm-test-5my44
call download-clang.bat
set LLD_BASE=llvm-test-57sv8
call download-lld.bat

cmake -BC:\projects\build -HC:\projects\src -GNinja ^
-DCMAKE_BUILD_TYPE=Release ^
-DCMAKE_INSTALL_PREFIX=C:\projects\install ^
-DLLVM_INCLUDE_EXAMPLES=OFF ^
-DLLVM_INCLUDE_TESTS=OFF ^
-DLLVM_TARGETS_TO_BUILD=X86 ^
-DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD=WebAssembly ^
-DCMAKE_C_FLAGS="/Gy /Gw" ^
-DCMAKE_CXX_FLAGS="/Gy /Gw" ^
-DCMAKE_C_COMPILER=clang-cl.exe ^
-DCMAKE_CXX_COMPILER=clang-cl.exe ^
-DCMAKE_EXE_LINKER_FLAGS="/OPT:REF /OPT:ICF" ^
-DCMAKE_SHARED_LINKER_FLAGS="/OPT:REF /OPT:ICF" ^
-DCMAKE_MODULE_LINKER_FLAGS="/OPT:REF /OPT:ICF" ^
-DCMAKE_LINKER=C:/projects/deps/lld/bin/lld-link.exe

ninja -C C:\projects\build -j4

ninja -C C:\projects\build install

cd C:\projects\install

7z a llvm.7z . -t7z

appveyor PushArtifact llvm.7z
