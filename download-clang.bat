appveyor DownloadFile https://ci.appveyor.com/api/projects/rongjiecomputer/%CLANG_BASE%/artifacts/clang-standalone.7z -FileName clang-standalone.7z

7z x clang-standalone.7z -oC:\projects\deps\clang > nul
copy C:\projects\deps\clang\bin\clang.exe C:\projects\deps\clang\bin\clang-cl.exe

set PATH=C:\projects\deps\clang\bin;%PATH%

rm clang-standalone.7z
