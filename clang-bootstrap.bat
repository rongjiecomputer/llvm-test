git clone --depth 1 https://git.llvm.org/git/clang.git C:\projects\src
rmdir /S /Q .git

set LLVM_BASE=llvm-test-v6vnr
call download-llvm.bat
set CLANG_BASE=llvm-test-jcdcd
call download-clang.bat

cmake -BC:\projects\build -HC:\projects\src -GNinja ^
-DCMAKE_BUILD_TYPE=Release ^
-DCMAKE_INSTALL_PREFIX=C:\projects\install ^
-DLLVM_INCLUDE_EXAMPLES=OFF ^
-DLLVM_INCLUDE_TESTS=OFF ^
-DLLVM_TARGETS_TO_BUILD=X86 ^
-DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD=WebAssembly ^
-DCMAKE_C_FLAGS="/Gy /Gw" ^
-DCMAKE_CXX_FLAGS="/Gy /Gw" ^
-DCMAKE_C_COMPILER=clang-cl.exe ^
-DCMAKE_CXX_COMPILER=clang-cl.exe ^
-DCMAKE_EXE_LINKER_FLAGS="/OPT:REF /OPT:ICF" ^
-DCMAKE_SHARED_LINKER_FLAGS="/OPT:REF /OPT:ICF" ^
-DCMAKE_MODULE_LINKER_FLAGS="/OPT:REF /OPT:ICF" ^
-DLLVM_CONFIG=C:\projects\deps\llvm\bin\llvm-config.exe

ninja -C C:\projects\build -j8

ninja -C C:\projects\build install-clang install-clang-headers

cd C:\projects\install

7z a clang-standalone.7z bin\clang.exe lib

appveyor PushArtifact clang-standalone.7z

rm clang-standalone.7z

ninja -C C:\projects\build install

7z a clang.7z .

appveyor PushArtifact clang.7z
